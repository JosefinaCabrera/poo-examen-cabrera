/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.examen;

/**
 *
 * @author jocab
 */
public class Proyecto {
    Integer id;
    String nombre;
    String monto;

    public Proyecto() {
    }

    public Proyecto(Integer id, String nombre, String monto) {
        this.id = id;
        this.nombre = nombre;
        this.monto = monto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }
    
    public void mostrar() {
        System.out.printf("|     %-11s |     %-11s |     %-23s|\n", getId(), getNombre(), getMonto());
    }

    @Override
    public String toString() {
        return "" + id + "\t     |" + nombre +"\t     |" + monto + "\n";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Proyecto other = (Proyecto) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
