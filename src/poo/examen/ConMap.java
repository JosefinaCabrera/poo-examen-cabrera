/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.examen;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import static poo.examen.ConSet.ANSI_GREEN;

/**
 *
 * @author jocab
 */
public class ConMap {
    Proyecto pro = new Proyecto();
    HashMap<Integer, Proyecto> ListaM = new HashMap<>();
    Scanner dato = new Scanner(System.in);
    Integer id;
    String nombre;
    String monto;
    
    void AgregarProyecto() {
        pro = new Proyecto();
        System.out.print("Ingrese Id: ");
        id = dato.nextInt();
        pro.setId(id);
        dato.nextLine();
        System.out.print("Ingrese Nombre: ");
        nombre = dato.nextLine();
        pro.setNombre(nombre);
        System.out.print("Ingrese Monto: ");
        monto = dato.nextLine();
        pro.setMonto(monto);
        Proyecto pro = new Proyecto(id, nombre, monto);
        ListaM.put(id, pro);
    }
    void EliminarProyecto() {
        System.out.print("Ingrese el N° de ID: ");
        Integer borrar = dato.nextInt();
        if (ListaM.containsKey(borrar)) {
            ListaM.remove(borrar);
            System.out.println("El Proyecto fue eliminado... Presione Enter!  ");
        } else {
            System.out.println("Proyecto no existe");

        }
    }
    void MostrarProyecto() {
        System.out.println(ANSI_GREEN + "+----------------------------------------------+");
        System.out.println("|                 LISTA DE PROYECTOS               |");
        System.out.println(ANSI_GREEN + "+----------------------------------------------+");
        System.out.println("|  ID   |    Apellido y Nombre      |    Monto     |");
        System.out.println(ANSI_GREEN + "|----------------------------------------------|");
        Iterator<Integer> it = ListaM.keySet().iterator();
        while (it.hasNext()) {
            Integer indice = it.next();
            System.out.println("| \t"  + ListaM.get(indice));
        }

        System.out.println(ANSI_GREEN + "+----------------------------------------------+");
        System.out.println("Pulse Enter para continuar!");
        dato.nextLine();
    }

}
