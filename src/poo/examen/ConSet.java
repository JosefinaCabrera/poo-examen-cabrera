/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.examen;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import javax.swing.JOptionPane;

/**
 *
 * @author jocab
 */
public class ConSet {
    public static final String ANSI_GREEN = "\u001B[32m";
    private Set<Empleado> listaEmp;
    Scanner dato = new Scanner(System.in);
    int dni;
    String ayn;
    String telefono;
    String mail;

    private Set<Empleado> listaE;
    private Set<Proyecto> listaP;
    private Set<Asignar> listaA;
    
    public ConSet() {
        listaEmp = new HashSet<>();
        
        listaE = new HashSet<>();
        listaP = new HashSet<>();
        listaA = new HashSet<>();
    }

    public void ListAgregarEmpleado(Empleado em) {
        listaEmp.add(em);
    }
   // AGREGAR EMPLEADO    
//    public Empleado addEmpleado() {
//        Empleado e = new Empleado();
//        System.out.print("Ingrese DNI: ");
//        dni = dato.nextInt();     
//        e.setDni(dni);        
//        dato.nextLine();
//        System.out.print("Ingrese el Apellido y Nombre: ");
//        ayn = dato.nextLine();
//        e.setAyn(ayn);
//        System.out.print("Ingrese Telefono: ");
//        telefono = dato.nextLine();
//        e.setTelefono(telefono);
//        System.out.print("Ingrese Email: ");
//        mail = dato.nextLine();
//        e.setMail(mail);       
//        return e;
//    }
    //EXCEPCIONES
    public Empleado addEmpleado() {
        Empleado e = new Empleado();
        try {
            System.out.print("Ingrese DNI: ");
        dni = dato.nextInt();     
        e.setDni(dni);        
        dato.nextLine();
        System.out.print("Ingrese el Apellido y Nombre: ");
        ayn = dato.nextLine();
        e.setAyn(ayn);
        System.out.print("Ingrese Telefono: ");
        telefono = dato.nextLine();
        e.setTelefono(telefono);
        System.out.print("Ingrese Email: ");
        mail = dato.nextLine();
        e.setMail(mail);       
        }catch(NumberFormatException n){
            JOptionPane.showMessageDialog(null,"No es Valido");            
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }
        return e;
    }
        
//    void ListEliminarEmpleado(Empleado empe) {
//
//        if (listaEmp.remove(empe)) {
//            System.out.println("El Empleado fue eliminado");
//        } else {
//            System.out.println("Empleado no existe");
//        }
//    }
    
    //EXCEPCIONES
    void ListEliminarEmpleado(Empleado empe) {

        if (listaEmp.remove(empe)) {
            System.out.println("El Empleado fue eliminado");
        } else {
            System.out.println("Empleado no existe");
        }
    }
    public Empleado borrarEmpleado() {
        Empleado empe = new Empleado();
        System.out.print("Ingrese Dni: ");
        dni = dato.nextInt();
        empe.setDni(dni);
        return empe;
    }

    
     public void agregarEmpleadoProyecto() {
        int aux = listaA.size();
        Asignar asig = new Asignar();
        int auxdni=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese el DNI del Empleado", "DNI",3));
        int auxid =Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el id de proyecto", "ID", 3));
        
        boolean ban1 = buscarEmp(auxdni);
          System.out.println(auxdni);
        boolean ban2 = buscarPro(auxid);
        if (ban1 && ban2){
            asig.setDniA(auxdni);
            asig.setIdA(auxid);
            listaA.add(asig);
            if (aux==listaA.size()){
                JOptionPane.showMessageDialog(null,"El Empleado ya esta en el Proyecto");
            }else{
                JOptionPane.showMessageDialog(null,"Empleado agregado al Proyecto con exito!!");
            } 
        }else{
            if (!ban1){
                JOptionPane.showMessageDialog(null,"El Empleado no existe");
            }else{
                JOptionPane.showMessageDialog(null,"El Proyecto no existe");
            }
        }        
      }
     private boolean buscarEmp(int auxdni) {
        boolean bande=false;
        for (Empleado e: listaE){
            if (auxdni==e.getDni()){
                bande=true;
            }
        }        
        return bande;
    }

    private boolean buscarPro(int auxid) {
        boolean bande=false;
        for (Proyecto p: listaP){
            if (auxid==p.getId()){
                bande=true;
            }
        }        
        return bande;
    }
      
     
//    public Empleado BuscarEstudiante() {
//        Estudiante aux = new Estudiante();
//        System.out.print("Ingrese numero de libreta: ");
//        lu = dato.nextInt();
//        aux.setLu(lu);
//
//        if (listaE2.contains(aux)) {
//            System.out.print("Estudiante si existe");
//            System.out.println("");
//            
//        } else {
//            System.out.print("Estudiante no existe");
//             System.out.println("");
//        }
//        return aux;
//    }

    void MostrarEmpleado() {

        System.out.println(ANSI_GREEN+"+---------------------------------------------------------------------------+");
        System.out.println("|             LISTA DE EMPLEADOS         ------------------                             |");
        System.out.println(ANSI_GREEN+"+--------------------------------------------------------------------------+");
        System.out.println("|      DNI       | Apellido y Nombre |     Telefono      |        Email         | ");
        System.out.println(ANSI_GREEN+"|--------------------------------------------------------------------------|");
        for (Empleado em : listaEmp) {
            em.mostrar();
        System.out.println(ANSI_GREEN+"---------------------------------------------------------------------------");
        }
        System.out.println("Pulse Enter para continuar!");
        dato.nextLine();
    }    
    
}
