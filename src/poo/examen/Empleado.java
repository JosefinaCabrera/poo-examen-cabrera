/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.examen;

//import java.util.HashSet;

import java.util.Objects;

//import java.util.Objects;
//import java.util.Set;
//import javax.swing.JOptionPane;

/**
 *
 * @author jocab
 */
public class Empleado {
    private int dni;
    private String ayn;
    private String telefono;
    private String mail;

    public Empleado() {
    }

    public Empleado(int dni, String ayn, String telefono, String mail) {
        this.dni = dni;
        this.ayn = ayn;
        this.telefono = telefono;
        this.mail = mail;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getAyn() {
        return ayn;
    }

    public void setAyn(String ayn) {
        this.ayn = ayn;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public void mostrar() {
        System.out.printf("|     %-10s |     %-11s |     %-16s |     %-17s|\n", getDni(), getAyn(), getTelefono(), getMail());
    }

    @Override
    public String toString() {
        return "" + dni + "\t     " + ayn + "\t     " + telefono + "\t     " + mail +"\t     "+ "\n";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.dni;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        if (this.dni != other.dni) {
            return false;
        }
        return true;
    }
    
    
    
    
    
  
//    @Override
//    public int hashCode() {
//        int hash = 3;
//        hash = 53 * hash + Objects.hashCode(this.dni);
//        return hash;
//    }
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Empleado other = (Empleado) obj;
//        if (!Objects.equals(this.dni, other.dni)) {
//            return false;
//        }
//        return true;
//    }
//    static Set<Empleado> ListaEmp = new HashSet<Empleado>();
//    
//     public void addCliente(Empleado emp) {
//        ListaEmp.add(emp);
//    }
//
//    //ADD CLIENTE
//    public Empleado nuevoEmpleado() {
//        Empleado emp = new Empleado();
//        emp.setDni(JOptionPane.showInputDialog(null, "Ingrese DNI"));
//        emp.setAyn(JOptionPane.showInputDialog(null, "Ingrese Apellido Y Nombre"));
//        emp.setTelefono(JOptionPane.showInputDialog(null, "Ingrese Telefono"));
//        emp.setMail(JOptionPane.showInputDialog(null, "Ingrese Email"));
//        return emp;
//    }
//    @Override
//    public String toString() {
//        JOptionPane.showMessageDialog(null, "DATOS EMPLEADO:  \n" + "DNI: " + dni
//                + "\n" + "Apellido y Nombre:  " + ayn + "\n" + "Telefono: " + telefono+ "\n" + "Email: "+mail);
//        return null;
//    }
//    public void verDatos(){
//        System.out.println("DATOS DEL EMPLEADO\n"
//                + "DNI : "+this.getDni()+"\n"
//                +"APELLIDO Y NOMBRE : "+this.getAyn()+"\n"
//                +"TELEFONO : "+this.getTelefono()+"\n"
//                +"MAIL : "+this.getMail()+"\n");
//    }

   
}
