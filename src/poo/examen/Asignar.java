/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.examen;

/**
 *
 * @author jocab
 */
public class Asignar {
    int dniA;
    Integer idA;

    public Asignar() {
    }

    public Asignar(int dniA, Integer idA) {
        this.dniA = dniA;
        this.idA = idA;
    }

    public int getDniA() {
        return dniA;
    }

    public void setDniA(int dniA) {
        this.dniA = dniA;
    }

    public Integer getIdA() {
        return idA;
    }

    public void setIdA(Integer idA) {
        this.idA = idA;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.idA;
        hash = 53 * hash + this.dniA;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Asignar other = (Asignar) obj;
        if (this.idA != other.idA) {
            return false;
        }
        if (this.dniA != other.dniA) {
            return false;
        }
        return true;
    }
    
    public void mostrarProyectosA(){
        System.out.printf("|    %-11s |      %-23s|"
                + "\n", getDniA(),getIdA());
        
    }
    
    
}

   
