/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.examen;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author jocab
 */
public class POOExamen {

    /**
     * @param args the command line arguments
     */
    static Set<Empleado> ListaEmp = new HashSet<Empleado>();
    
    public static void main(String[] args) {
        // TODO code application logic here
        Empleado em = new Empleado();
        Scanner dato = new Scanner(System.in);
        ConSet St = new ConSet();
        ConMap Mp = new ConMap();
        //Empleado dat = new Empleado();
        Scanner entrada = new Scanner(System.in);
        //int tam;
        ConSet listaE = new ConSet();
        ConSet listaP = new ConSet();
        ConSet listaA = new ConSet();
        
        int op1, op2=0, op3=0;
        do {            
            System.out.println("---MENU PRINCIPAL ---");
            System.out.println("1. Empleados");
            System.out.println("2. Proyectos");
            System.out.println("3. Salir");
            System.out.println("Elija una opcion");
            op1 = entrada.nextInt();
            
            switch(op1){
                case 1:
                    do {
                        System.out.println("--------MENU EMPLEADOS-------");
                        System.out.println("1. Agregar Empleado");
                        System.out.println("2. Eliminar Empleado");
                        System.out.println("3. Listar Empleado");
                        System.out.println("4. Salir"); 
                        System.out.println("Elija una opcion"); 
                        op2 = entrada.nextInt();
                                                                     
                        switch(op2){
                            case 1:
                                em = St.addEmpleado();
                                St.ListAgregarEmpleado(em);
                                break;
                            case 2:
                                em = St.borrarEmpleado();
                                St.ListEliminarEmpleado(em);                              
                                break;
                            case 3:
                                St.MostrarEmpleado();
                                System.out.println("\n\n\n");
                                break;
                            case 4:
                                System.out.println("Salir");
                                break;
                            default:
                                    System.out.println("Opcion Incorrecta...");   
                                    break;                                  
                        }
                    } while (op2!=4);
                case 2:
                    do {  
                        System.out.println("--------MENU PROYECTOS-------");
                        System.out.println("1. Agregar Nuevo Proyecto");
                        System.out.println("2. Eliminar Proyecto");
                        System.out.println("3. Listar Proyectos");
                        System.out.println("4. Agregar Empleado a Proyecto");
                        //System.out.println("5. Quitar Empleado a Proyecto");
                        //System.out.println("6. Listar Datos de Proyecto");
                        //1System.out.println("7. Calcular montos destinados a Proyectos");
                        System.out.println("8. Salir");
                        System.out.println("Elija una opcion");
                        op3 = entrada.nextInt();
                        int p = 0;
                        switch(op3){
                            case 1:
                                p++;
                                Mp.AgregarProyecto();
                                break;
                            case 2:
                                Mp.EliminarProyecto();
                                dato.nextLine();
                                break;
                            case 3:
                                Mp.MostrarProyecto();
                                break;
                            case 4:
                                listaA.agregarEmpleadoProyecto();
                                break;
                            case 5:
                                break;
                            case 6:
                                break;
                            case 7:
                                break;
                            case 8:
                                System.out.println("Salir");
                                break;
                                default:
                                    System.out.println("Opcion Incorrecta...");   
                                    break;                           
                        }
                    } while (op3!=8);
                    break;
                case 3:
                    System.out.println("Programa Finalizado");
                    break;
                    default:
                      System.out.println("Opcion Incorrecta...");   
                        break;
                
            }
        } while (op1!=3);
    }
    
    
}
